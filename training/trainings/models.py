import json

from django.db import models


class Tutor(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Training(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    tutor = models.ForeignKey(Tutor, on_delete=models.SET_NULL, null=True)
    first_date = models.DateField(null=True)
    second_date = models.DateField(null=True)

    def set_date(self, x):
        self.date = json.dumps(x)

    def get_date(self):
        return json.loads(self.date)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.second_date:
            self.second_date = self.first_date
        super(Training, self).save(*args, **kwargs)


class Appointment(models.Model):
    first_name = models.CharField(max_length=200)
    second_name = models.CharField(max_length=200)
    middle_name = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=200)
    training = models.ForeignKey(Training, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return "{} {}".format(self.second_name, self.phone_number)
