from django.contrib import admin

from trainings.models import Training, Tutor, Appointment


class TrainingsInline(admin.TabularInline):
    model = Training


class TutorAdmin(admin.ModelAdmin):
    pass


admin.site.register(Training)
admin.site.register(Tutor, TutorAdmin)
admin.site.register(Appointment)


