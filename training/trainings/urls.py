from django.conf.urls import url

from .views import get_tutors, get_trainings, post_appointment

urlpatterns = [
    url('tutors', get_tutors, name='get_tutors'),
    url('trainings', get_trainings, name='get_trainings'),
    url('appointment', post_appointment, name='appointment'),
]