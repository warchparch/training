from json import loads

from django.shortcuts import HttpResponse
from django.http import Http404
from django import forms


from .models import Tutor, Training, Appointment


class AppointmentValidationForm(forms.Form):
    first_name = forms.CharField()
    second_name = forms.CharField()
    middle_name = forms.CharField()
    phone_number = forms.CharField()
    training = forms.CharField()


def get_tutors(request):
    tutors = Tutor.objects.all()
    tutors = [tutor.name for tutor in tutors]
    return HttpResponse(tutors)


def get_trainings(request):
    tutor = request.GET.get("tutor")
    first_date = request.GET.get("first_date")
    second_date = request.GET.get("second_date")

    query = Training.objects

    if tutor:
        query = Training.objects.filter(tutor__name=tutor)
    if first_date:
        if second_date:
            query = query.filter(first_date__range=(first_date, second_date))
        else:
            query = query.filter(first_date=first_date)
    if not tutor and not first_date and not second_date:
        query = query.all()
    trainings = [training.name for training in query]
    return HttpResponse(trainings)


def post_appointment(request):

    data = loads(request.body)

    if request.method != "POST" \
            or not AppointmentValidationForm(data).is_valid() \
            or not Training.objects.filter(name=data.get("training")).exists():
        raise Http404

    new_appointment = Appointment(first_name=data.get("first_name"),
                                  second_name=data.get("second_name"),
                                  middle_name=data.get("middle_name"),
                                  phone_number=data.get("phone_number"),
                                  training=Training.objects.get(name=data.get("training")))
    new_appointment.save()

    return HttpResponse("Appointment created")
